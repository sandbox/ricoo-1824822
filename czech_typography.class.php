<?php

/**
 * @file czech_typography.class.php
 * Defines a class for providing czech typographical tweaks
 */
class CzechTypography {

  public static function orphanLetters($text) {
    $finder = "/
    	(\s+|^|&nbsp;|<\/?(?:p|div|pre)[^>]*>)					# pre-preposition-entity (space, nb-space, line-start, block-elm-start)
			(<\/?(?:a|em|span|strong|i|b)[^>]*>)*						# pre-preposition-tag(s)
			([a-zA-Z])																			# lonely letter (preposition)
			((?:<\/?(?:a|em|span|strong|i|b)[^>]*>\s*)*)		# pre-space-tag(s)
			\s+																							# the space
			(\S+)																						# the word
			/imx";
		$replace = '$1$2$3$4&nbsp;$5';
		// Foreach to handle overlapping matches (more prepositions in a row)
    for($count = 1; $count > 0; $text = preg_replace($finder, $replace, $text, -1, $count)) {};
    return $text;
  }

  /**
   * czech_typography
   * 
   * The super typography filter.
   */
  public static function filter($text) {
    $text = CzechTypography::orphanLetters($text);
    return $text;
  }
}

